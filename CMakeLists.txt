# Top level CMakeLists.txt file for FEniCS Solid Mechanics

# Require CMake 2.8
cmake_minimum_required(VERSION 2.8)

#------------------------------------------------------------------------------
# Set project name and version number

project(fenics-solid-mechanics)
set(FENICS_SOLID_MECHANICS_VERSION_MAJOR "0")
set(FENICS_SOLID_MECHANICS_VERSION_MINOR "0")
set(FENICS_SOLID_MECHANICS_VERSION_MICRO "1+")
set(FENICS_SOLID_MECHANICS_VERSION "${FENICS_SOLID_MECHANICS_VERSION_MAJOR}.${FENICS_SOLID_MECHANICS_VERSION_MINOR}.${FENICS_SOLID_MECHANICS_VERSION_MICRO}")

#------------------------------------------------------------------------------
# General configuration

# Set CMake options, see `cmake --help-policy CMP000x`
if (COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif()

set(FENICS_SOLID_MECHANICS_CMAKE_DIR "${fenics-solid-mechanics_SOURCE_DIR}/cmake" CACHE INTERNAL "")

#------------------------------------------------------------------------------
# Get GIT changeset, if available

# Check for git
find_program(GIT_FOUND git)

if (GIT_FOUND)
  # Get the commit hash of the working branch
  execute_process(COMMAND git rev-parse HEAD
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_COMMIT_HASH
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
else()
  set(GIT_COMMIT_HASH "unknown")
endif()

add_definitions(-DFENICSSOLID_GIT_COMMIT_HASH="${GIT_COMMIT_HASH}")

#------------------------------------------------------------------------------
# Configurable options for how we want to build

option(BUILD_SHARED_LIBS "Build FEniCS Solid Mechanics with shared libraries."
  ON)

#------------------------------------------------------------------------------
# Compiler flags

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG(-std=c++11 HAVE_STD_CPP11)
if (HAVE_STD_CPP11)
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
else()
  CHECK_CXX_COMPILER_FLAG(-std=c++0x HAVE_STD_CPP0x)
  if (HAVE_STD_CPP0x)
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
  endif()
endif()

#------------------------------------------------------------------------------
# Find DOLFIN

find_package(DOLFIN REQUIRED)
include(${DOLFIN_USE_FILE})

#------------------------------------------------------------------------------
# Find VTK

# Need to get VTK config because VTK uses advanced VTK features which
# mean it's not enough to just link to the DOLFIN target. See
# http://www.vtk.org/pipermail/vtk-developers/2013-October/014402.html
find_package(VTK HINTS ${VTK_DIR} $ENV{VTK_DIR} NO_MODULE QUIET)

#------------------------------------------------------------------------------
# Installation of FENiCS Solid Mechanics library

# Set DOLFIN install sub-directories
set(FENICS_SOLID_MECHANICS_BIN_DIR "bin" CACHE PATH
  "Binary installation directory.")
set(FENICS_SOLID_MECHANICS_LIB_DIR "lib" CACHE PATH
  "Library installation directory.")
set(FENICS_SOLID_MECHANICS_INCLUDE_DIR "include/fenics-solid-mechanics" CACHE
  PATH "C/C++ header installation directory.")
set(FENICS_SOLID_MECHANICS_SHARE_DIR "share/fenics-solid-mechanics" CACHE
  PATH "Shared data installation directory.")

# Add source directory
add_subdirectory(src)

#------------------------------------------------------------------------------
# Generate and install helper file dolfin.conf

# FIXME: Can CMake provide the library path name variable?
if (APPLE)
  set(OS_LIBRARY_PATH_NAME "DYLD_LIBRARY_PATH")
else()
  set(OS_LIBRARY_PATH_NAME "LD_LIBRARY_PATH")
endif()
#
# FIXME: not cross-platform compatible
# Create and install dolfin.conf file
configure_file(${FENICS_SOLID_MECHANICS_CMAKE_DIR}/fenics-solid-mechanics.conf.in
               ${CMAKE_BINARY_DIR}/fenics-solid-mechanics.conf @ONLY)
install(FILES ${CMAKE_BINARY_DIR}/fenics-solid-mechanics.conf
        DESTINATION ${FENICS_SOLID_MECHANICS_SHARE_DIR}
        COMPONENT Development)

#------------------------------------------------------------------------------
# Print post-install message

add_subdirectory(cmake/post-install)

#------------------------------------------------------------------------------
