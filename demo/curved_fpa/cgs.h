/*
 * cgs.h
 *
 *  Created on: May 11, 2018
 *      Author: Chaffra Affouda
 */

#ifndef DEMO_DOCUMENTED_DRIFT_DIFFUSION_CPP_CGS_H_
#define DEMO_DOCUMENTED_DRIFT_DIFFUSION_CPP_CGS_H_

//cgs units
const double cm = 1.0;
const double m = 100.0*cm;
const double mum = 1e-6*m;
const double nm = 1e-9*m;

const double g = 1.0;
const double kg = 1000.0*g;

const double s = 1.0;
const double ns = 1e-9*s;
const double mus = 1e-6*s;

const double erg = g*cm*cm/s/s;
const double Joule = kg*m*m/s/s;

const double Watt = Joule/s;
const double mW = 1e-3*Watt;

const double Coulomb = 1.0;
const double Ampere = Coulomb/s;
const double Kelvin = 1.0;

const double Eps0 = 8.854187817620e-12*std::pow(Ampere,2)*std::pow(s,4)/kg/std::pow(m,3);
const double h_planck = 6.62606885e-27*erg*s;
const double hbar_planck = h_planck/2.0/M_PI;
const double q_e = 1.6021766208e-19*Coulomb;
const double k_b = 1.38064852e-16*erg/Kelvin;
const double m_e = 9.10938356e-31*kg;

const double eV =q_e*Joule/Coulomb;
const double Volt = Joule/Coulomb;
const double mV = 1e-3*Volt;

const double Pi = 3.14159265358979323846;

const double Pascal = kg/m/s/s;
const double MPa = 1e6*Pascal;
const double GPa = 1e9*Pascal;
const double atm = 101325*Pascal;

#endif /* DEMO_DOCUMENTED_DRIFT_DIFFUSION_CPP_CGS_H_ */
