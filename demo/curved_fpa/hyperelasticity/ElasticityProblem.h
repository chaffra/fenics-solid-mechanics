// Copyright (C) 2006-2011 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2011-02-06

#ifndef __ELASTICITY_PROBLEM_H
#define __ELASTICITY_PROBLEM_H

#include <memory>
#include <vector>
#include <dolfin/nls/NonlinearProblem.h>
#include <dolfin/parameter/Parameters.h>

namespace dolfin
{
  class DirichletBC;
  class GenericDofMap;
  class GenericMatrix;
  class GenericVector;
  class Form;
  class Function;
  class NewtonSolver;
  class SystemAssembler;
}

namespace semifem
{

  class ElasticityProblem : public dolfin::NonlinearProblem
  {
  public:

    /// Delete copy constructor and assignement
    ElasticityProblem& operator=(const ElasticityProblem&) = delete;  // Disallow copying
    ElasticityProblem(const ElasticityProblem&) = delete;

    /// Constructor
    ElasticityProblem(std::shared_ptr<const dolfin::Form> a,
                      std::shared_ptr<const dolfin::Form> L,
                      std::shared_ptr<dolfin::Function> u,
                      const std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs);

    /// Constructor
    ElasticityProblem(std::shared_ptr<const dolfin::Form> a,
                      std::shared_ptr<const dolfin::Form> L,
                      std::shared_ptr<dolfin::Function> u,
                      const std::vector<std::shared_ptr<const dolfin::DirichletBC>> bcs,
                      std::shared_ptr<dolfin::NewtonSolver> newton_solver);

    /// Destructor
    ~ElasticityProblem();

    /// Loop quadrature points and compute local tangents and stresses
    void form(dolfin::GenericMatrix& A, dolfin::GenericMatrix& P,
              dolfin::GenericVector& b, const dolfin::GenericVector& x);

    /// User defined assemble of residual vector
    void F(dolfin::GenericVector& b, const dolfin::GenericVector& x);

    /// User defined assemble of Jacobian matrix
    void J(dolfin::GenericMatrix& A, const dolfin::GenericVector& x);

    /// Parameters
    dolfin::Parameters parameters;

  private:

    // For system after constitutive update
    void form_tensors(dolfin::GenericMatrix& A, dolfin::GenericVector& b,
                      const dolfin::GenericVector& x);

    // Assembler
    dolfin::SystemAssembler _assembler;

    // Displacement
    std::shared_ptr<const dolfin::Function> _u;

    // The Newton solver
    std::shared_ptr<dolfin::NewtonSolver> _newton_solver;

  };
}

#endif
