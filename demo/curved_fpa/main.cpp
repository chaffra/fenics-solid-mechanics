// Copyright (C) 2006-2011 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2011-02-06

#include <dolfin.h>
#include <FenicsSolidMechanics.h>

// Switch between linear and quadratic elements
#include "Plas3D.h"
//#include "../forms/p1_forms/Plas3D.h"

#include "cgs.h"

using namespace dolfin;

// Sub domain for Dirichlet boundary condition
//class DirichletBoundaryX1 : public SubDomain
//{
//  bool inside(Eigen::Ref<const Eigen::VectorXd> x, bool on_boundary) const
//  { return near(x[0],1.0*cm) && on_boundary; }
//};
//
//// Sub domain for Dirichlet boundary condition
//class DirichletBoundaryX0 : public SubDomain
//{
//  bool inside(Eigen::Ref<const Eigen::VectorXd> x, bool on_boundary) const
//  { return near(x[0], 0.0) && on_boundary; }
//};

class SideSurface : public SubDomain
{
  bool inside(Eigen::Ref<const Eigen::VectorXd> x, bool on_boundary) const
  {
	  bool val = near(x[0], -0.5*cm);
	  val = val or near(x[0], 0.5*cm);
	  val = val or near(x[1], -0.5*cm);
	  val = val or near(x[1], 0.5*cm);
	  val = val and on_boundary;
	  return val; }
};

class Corners : public SubDomain
{
public:
  Corners(const std::vector<Point>& points, double tol) : SubDomain(),
		  _points(points), _tol(tol) {}
  bool inside(Eigen::Ref<const Eigen::VectorXd> x, bool on_boundary) const
  {
	  Point p(x[0],x[1]);
	  for(auto it=_points.begin(); it!= _points.end(); ++it){
		  if( it->distance(p) < _tol) return true;
	  }

	  return false;
  }

private:
  double _tol;
  const std::vector<Point>& _points;
};

// Sub domain for boundary condition
class TopSurface : public SubDomain
{

public:
	TopSurface() : SubDomain() {}
  bool inside(Eigen::Ref<const Eigen::VectorXd> x, bool on_boundary) const
  {
	  return on_boundary && near(x[2], 50*mum); }

};

class BottomSurface : public SubDomain
{

public:
	BottomSurface() : SubDomain() {}
  bool inside(Eigen::Ref<const Eigen::VectorXd> x, bool on_boundary) const
  {
	  return on_boundary && near(x[2], 0*mum);
  }
};

class BottomCenter : public SubDomain
{

public:
	BottomCenter(double tol) : SubDomain(),
	_center(0.0*cm, 0.0*cm, 0.0*mum),
	_tol(tol) {}
  bool inside(Eigen::Ref<const Eigen::VectorXd> x, bool on_boundary) const
  {
	  Point p(x[0], x[1], x[2]);
	  return (p.distance(_center) < _tol) && on_boundary;
  }

private:
  const Point _center;
  double _tol;
};

class PetzvalSurface: public Expression
{
public:
	PetzvalSurface(double f, double n) :
				Expression(), _f(f), _n(n), _center(0.0*cm, 0.0*cm, 0.0*mum) {};

	void eval(Eigen::Ref<Eigen::VectorXd> values,
	  	                      Eigen::Ref<const Eigen::VectorXd> x) const override {

		//values[0] = 0.0;
		//values[1] = 0.0;

		double x0 = x[0]-_center[0];
		double x1 = x[1]-_center[1];

		double r2 = x0*x0+x1*x1;
		//double rmax = 2.0*cm*std::sqrt(2.0);
		//double offset = rmax*rmax/_n/_f/2.0;

		values[0] = r2/_n/_f/2.0; //-offset;
	}

	double offset(double rmax){
		return rmax*rmax/_n/_f/2.0;
	}

private:
	double _f;
	double _n;
	const Point _center;


};

class BoundaryPressure: public Expression
{
public:
	BoundaryPressure(std::shared_ptr<SubDomain> boundary,
			std::shared_ptr<GenericFunction> pressure) :
				Expression(3), _boundary(boundary), _pressure(pressure) {};

	void eval(Eigen::Ref<Eigen::VectorXd> values,
	  	                      Eigen::Ref<const Eigen::VectorXd> x) const override {

		if(_boundary->inside(x,true)){
			_pressure->eval(values,x);
		}
		else {
			values[0] = 0.0;
			values[1] = 0.0;
			values[2] = 0.0;
		}
	}

private:
	std::shared_ptr<SubDomain> _boundary;
	std::shared_ptr<GenericFunction> _pressure;

};

int main()
{
	dolfin::parameters["linear_algebra_backend"] = "Eigen";
  Timer timer("Total plasicity solver time");

  //dolfin::parameters["reorder_dofs_serial"] = false;
  //dolfin::parameters["dof_ordering_library"] = "SCOTCH";
  //dolfin::parameters["linear_algebra_backend"] = "Epetra";


  Point p0(-2.0*cm,-2.0*cm,0.0);
  Point p1(2.0*cm, 2.0*cm, 50*mum);
  // Create mesh
  auto mesh = std::make_shared<BoxMesh>(p0, p1, 24, 24, 3);

  // Young's modulus and Poisson's ratio
  double E = 170.0*GPa;
  double nu = 0.27;


  // Source terms, RHS
  auto f = std::make_shared<Constant>(0.0, 0.0, 0.0);

  auto top_surface = std::make_shared<TopSurface>();
  auto p = std::make_shared<Constant>(0.0, 0.0, 0.0*atm);
  auto top_surface_pressure = std::make_shared<BoundaryPressure>(top_surface, p);

  // Function spaces
  auto V = std::make_shared<Plas3D::Form_a::TestSpace>(mesh);
  dolfin::cout << "Number of dofs: " << V->dim() << dolfin::endl;

  // Extract elements for stress and tangent
  auto Vt = std::make_shared<Plas3D::Form_a::CoefficientSpace_t>(mesh);
  auto element_t = Vt->element();

  auto Vs = std::make_shared<Plas3D::Form_L::CoefficientSpace_s>(mesh);
  auto element_s = Vs->element();

  // Create boundary conditions (use SubSpace to apply simply
  // supported BCs)
  auto zero = std::make_shared<Constant>(0.0, 0.0, 0.0);

  Point p00(-2*cm,-2*cm, 0);
  Point p01(-2*cm,2*cm, 0);
  Point p02(2*cm,-2*cm, 0);
  Point p03(2*cm,2*cm, 0);

  std::vector<Point> _bottom_corners = {p00,p01,p02,p03};

  auto bottom_corners = std::make_shared<Corners>( _bottom_corners,mesh->hmin()*0.5);
  auto bc_bottom_corners = std::make_shared<DirichletBC>(V,
		  zero, bottom_corners, "topological");

  auto Vz = V->sub(2);
  double rmax = 2.0*cm*std::sqrt(2.0);
  auto petzval_surface = std::make_shared<PetzvalSurface>(2*cm, 2.61);
  auto fixed_domain = std::make_shared<BottomSurface>();
  auto bc_fixed_domain = std::make_shared<DirichletBC>(Vz,
		  petzval_surface, fixed_domain);

  auto bottom_center = std::make_shared<BottomCenter>(2*mesh->hmin());
//  auto bottom_center_pos = std::make_shared<Constant>(0.0, 0.0,
//		  -petzval_surface->offset(rmax));
  auto bc_bottom_center = std::make_shared<DirichletBC>(V, zero,
		  bottom_center, "topological");


  std::vector<std::shared_ptr<const DirichletBC>> bcs = {
		  bc_fixed_domain,
		  //bc_bottom_center
		  //bc_bottom_corners,

  };

  // Slope of hardening (linear) and hardening parameter
  const double E_t = 0.01*E;
  const double hardening_parameter = E_t/(1.0 - E_t/E);

  // Yield stress
  const double yield_stress = 7.0*GPa;

  // Solution function
  auto u = std::make_shared<Function>(V);

  // Object of class von Mises
  auto J2 = std::make_shared<const fenicssolid::VonMises>(E, nu, yield_stress,
                                                          hardening_parameter);

  // Constituive update
  auto constitutive_update
    = std::make_shared<fenicssolid::ConstitutiveUpdate>(u, element_s,
                                                       Vs->dofmap(), J2);

  // Create forms and attach functions
  auto tangent
    = std::make_shared<fenicssolid::QuadratureFunction>(mesh, element_t,
                                                        constitutive_update,
                                                        constitutive_update->w_tangent());

  auto a = std::make_shared<Plas3D::Form_a>(V, V);
  a->t = tangent;

  auto L = std::make_shared<Plas3D::Form_L>(V);
  L->f = f;
  L->T = zero;
  auto stress = std::make_shared<fenicssolid::QuadratureFunction>(mesh, element_s,
		  	  	  	  	  	  	  	  	 //constitutive_update,
                                         constitutive_update->w_stress());
  L->s = stress;

  // Create PlasticityProblem
  auto nonlinear_problem
    = std::make_shared<fenicssolid::PlasticityProblem>(a, L, u, tangent,
                                                       stress, bcs, J2);

  auto Veps = std::make_shared<Plas3D::Form_a_eps::TestSpace>(mesh);
  auto a_eps = std::make_shared<Plas3D::Form_a_eps>(Veps, Veps);
  auto L_eps = std::make_shared<Plas3D::Form_L_eps>(Veps);
  L_eps->disp = u;
  auto elastic_strain = std::make_shared<Function>(Veps);
  elastic_strain->rename("eps","elastic_strain");

  auto _E = std::make_shared<Constant>(E);
  auto _nu = std::make_shared<Constant>(nu);
  auto stress_scale = std::make_shared<Constant>(MPa);

  auto Vstress = std::make_shared<Plas3D::Form_a_s::TestSpace>(mesh);
  auto a_s = std::make_shared<Plas3D::Form_a_s>(Vstress, Vstress);
  auto L_s = std::make_shared<Plas3D::Form_L_s>(Vstress);
  L_s->disp = u;
  L_s->E = _E;
  L_s->nu = _nu;
  L_s->stress_scale = stress_scale;
  auto elastic_stress = std::make_shared<Function>(Vstress);
  elastic_stress->rename("sigma","elastic_stress");



  // Create nonlinear solver and set parameters
  dolfin::NewtonSolver nonlinear_solver;
  nonlinear_solver.parameters["convergence_criterion"] = "residual";
  nonlinear_solver.parameters["maximum_iterations"]    = 10;
  nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-16;
  nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-16;
  nonlinear_solver.parameters["error_on_nonconvergence"] = false;

  nonlinear_solver.parameters["relaxation_parameter"] = 1.0;
  //nonlinear_solver.parameters["threshold_parameter"] = 10*mum;

  nonlinear_solver.parameters["linear_solver"] = "pardiso";

  // File names for output
  File file1("output/disp.pvd");
  File file2("output/eq_plas_strain.pvd");

  File stress_file("output/stress.pvd");
  File strain_file("output/strain.pvd");
  File traction_file("output/traction.pvd");

  // Equivalent plastic strain for visualisation
  auto eps_eq = std::make_shared<CellFunction<double>>(mesh);

  // Solve non-linear problem
  nonlinear_solver.solve(*nonlinear_problem, *(u->vector()));

  //auto deformed_mesh = std::make_shared<Mesh>(*mesh);
  //deformed_mesh->geometry().init(mesh->geometry().dim(), 2);
  //ALE::move(*deformed_mesh,*u);

  // Update variables
  constitutive_update->update_history();



  auto Vtraction = std::make_shared<Plas3D::Form_a_traction::TestSpace>(mesh);
  auto a_traction = std::make_shared<Plas3D::Form_a_traction>(Vtraction, Vtraction);
  auto L_traction = std::make_shared<Plas3D::Form_L_traction>(Vtraction);
  L_traction->disp = u;
  L_traction->E = _E;
  L_traction->nu = _nu;
  L_traction->stress_scale = stress_scale;
  auto traction = std::make_shared<Function>(Vtraction);
  traction->rename("traction","traction");

  // Write output to files
  file1 << *u;
  constitutive_update->eps_p_eq().compute_mean(eps_eq);
  file2 << *eps_eq;


  // Compute strain for visualisation
  LocalSolver strain_solver(a_eps, L_eps, LocalSolver::SolverType::LU);
  strain_solver.solve_local_rhs(*elastic_strain);
  //solve( *a_eps == *L_eps, *elastic_strain);
  strain_file << *elastic_strain;

  // Compute stress for visualisation
  LocalSolver stress_solver(a_s, L_s, LocalSolver::SolverType::LU);
  stress_solver.solve_local_rhs(*elastic_stress);
  //solve( *a_eps == *L_eps, *elastic_strain);
  stress_file << *elastic_stress;

  // Compute traction for visualisation
  //LocalSolver traction_solver(a_traction, L_traction, LocalSolver::SolverType::LU);
  //traction_solver.solve_local_rhs(*traction);
  solve( *a_traction == *L_traction, *traction);
  traction_file << *traction;

  cout << "Solution norm: " << u->vector()->norm("l2") << endl;

  timer.stop();
  dolfin::list_timings(dolfin::TimingClear::clear, {dolfin::TimingType::wall});

  return 0;
}
